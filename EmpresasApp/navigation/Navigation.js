import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';

//Import de telas criadas
import LoginScreen from '../screens/LoginScreen';
import ListarEmpresasScreen from '../screens/ListarEmpresasScreen';
import SwitchLogin from '../screens/SwitchLogin';
import EmpresaInfoScreen from '../screens/EmpresaInfoScreen';
import ContatoEmpresa from '../screens/ContatoEmpresa';

// Paleta de cores
import {default as Colors} from '../constants/Colors';
import LogoutScreen from '../screens/LogoutScreen';

const MainNavigator = createStackNavigator({
    ListarEmpresas: ListarEmpresasScreen,
    EmpresaInfo: {
        screen: createMaterialTopTabNavigator(
            {
                Descricao: {
                    screen: EmpresaInfoScreen
                },
                Contato: {
                    screen: ContatoEmpresa
                }
            },
            {
                tabBarOptions:{
                    activeTintColor: '#000000',
                    inactiveTintColor: '#000000',
                    style:{
                        backgroundColor: Colors.primaryLight,
                        color: 'black'
                    },
                }
            }
        ),
        navigationOptions: ({ navigation }) => ({
            title: "Informações da Empresa",
            headerStyle: {
                backgroundColor: Colors.primary,
            },
            cardStyle: { backgroundColor: '#FFFFFF' }
        }),
    },
    Logout: LogoutScreen
});

const SignNavigator = createStackNavigator({
    Login: LoginScreen,
    Logout: LogoutScreen
});

const App = createSwitchNavigator({
    Switch: SwitchLogin,
    Login: SignNavigator,
    App: MainNavigator,
},{
    initialRouteName: 'Switch',
})
const NavigationApp = createAppContainer(App);

export default NavigationApp;