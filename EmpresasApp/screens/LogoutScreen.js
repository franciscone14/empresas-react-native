import React from 'react';
import {
    Alert,
    ActivityIndicator
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import Colors from '../constants/Colors';

class LogoutScreen extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerStyle: {
                backgroundColor: Colors.primary,
            }
        };
    };

    constructor(props){
        super(props);
        this.state = this.props.login;
    }

    componentDidMount(){
        this.deleteData();
    }

    deleteData = async() => {
        try {
            await AsyncStorage.removeItem('@access_token');
            await AsyncStorage.removeItem('@client');
            await AsyncStorage.removeItem('@uid');
            this.props.navigation.navigate('Login');
        } catch (e) {
            Alert.alert('Ops :(', 'Erro ao acessar o armazenamento, verifique suas permissões !');
        }
    }

    render(){
        return(
            <ActivityIndicator size="large" color={Colors.primaryLight}/>
        );
    }
}

function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(LogoutScreen);
