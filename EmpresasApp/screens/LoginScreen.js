import React from 'react';
import {
    View,
    Button,
    Image,
    StyleSheet,
    Alert
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import BasicInput from '../components/BasicInput';
import Colors from '../constants/Colors';
import api from '../services/api';

class LoginScreen extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerStyle: {
                backgroundColor: Colors.primary,
            }
        };
    };

    constructor(props){
        super(props);
        this.state = this.props.login;
    }

    saveData = async(access_token, client, uid) => {
        try {
            await AsyncStorage.setItem('@access_token', access_token);
            await AsyncStorage.setItem('@client', client);
            await AsyncStorage.setItem('@uid', uid);
        } catch (e) {
            Alert.alert('Ops :(', 'Erro ao acessar o armazenamento, verifique suas permissões !');
        }
    }

    loginRequest = () => {
        const {email, password} = this.state;

        api.post('api/v1/users/auth/sign_in',{
            email: email,
            password: password
        },{
            headers: {
                'Content-Type': 'application/json',
            }
        })
        .then(res => {
            const access_token = res.headers['access-token'];
            const client = res.headers['client'];
            const uid = res.headers['uid'];

            this.props.dispatch({
                type: 'SET_STATE',
                access_token: access_token,
                client: client,
                uid: uid
            });
            
            this.saveData(access_token, client, uid);
            this.props.navigation.navigate('App');
            
        })
        .catch(error => {
            Alert.alert("Ops :(", "Algo deu errado, por favor confira seus dados e tente novamente !");
        })
    }

    render(){
        return(
            <View style={styles.container}>
                <View style={{
                    alignContent: 'center',
                    margin: 10
                }}>
                    <Image
                        style={{alignSelf: 'center'}}
                        source={require('../assets/logo_ioasys.png')}
                    />
                </View>
                <View style={styles.inputArea}>
                    <BasicInput
                        label="E-mail:"
                        onChangeText={(email) => this.setState({email})}
                        value={this.state.email}
                        textContentType="emailAddress"
                    />
                    <BasicInput
                        label="Senha:"
                        onChangeText={(password) => this.setState({password})}
                        value={this.state.password}
                        textContentType="password"
                        secureTextEntry={true}
                    />
                </View>
                <View style={styles.buttonArea}>
                    <Button 
                        title="Login"
                        style={styles.button}
                        onPress={this.loginRequest}
                        color={Colors.primaryDark}
                    />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state){
    return {
        login: state.login,
        auth: state.auth
    }
}

export default connect(mapStateToProps)(LoginScreen);

const styles = StyleSheet.create({
    container:{
        flex:1,
        flexDirection: 'column',
        backgroundColor: 'white'
    },
    inputArea:{
        flexDirection: 'column',
        margin: 5,
        marginHorizontal: 20,
        alignContent: 'center'
    },
    buttonArea:{
        flex:1,
        margin: 5,
        marginHorizontal: 20,
    },
    button:{
        color: Colors.primary
    }
});