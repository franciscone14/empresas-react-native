import React from 'react';
import {
    View,
    Button,
    Text,
    Image,
    StyleSheet,
    ActivityIndicator
} from 'react-native';
import {connect} from 'react-redux';

import {default as Colors} from '../constants/Colors';

import api from '../services/api';

class EmpresaInfoScreen extends React.Component{

    static navigationOptions = ({navigation}) => {
        return {
            headerStyle: {
                backgroundColor: Colors.primary,
            }
        };
    };

    constructor(props){
        super(props);
        this.state = {
            empresa: {}
        }
    }

    componentDidMount(){
        this.doRequest();
    }

    doRequest = () => {
        const {access_token, client, uid} = this.props.auth;
        // Retorna o id da empresa selecionada na lista
        const id = this.props.navigation.getParam('id', 0);
        
        //Busca na api os dados da empresa com id = id
        api.get('api/v1/enterprises/' + id,{
            headers: {
                'Content-Type': 'application/json',
                'access-token': access_token,
                'client': client,
                'uid': uid
            }
        })
        .then(res => {
            this.setState({
                empresa: res.data.enterprise
            })
        })
        .catch(error => {
            /**
             * Se o erro for 401 (Não autorizado), então as credencias não são mais
             * validas, logo o usuário deve realizar login novamente
             */
            if(error.response && error.response.status === 401){
                Alert.alert('Ops :(', 'Sua sessão expirou, faça login novamente !');
                this.props.navigation.navigate('Logout');
            }
        });
    }

    render(){
        const {empresa} = this.state;
        if(empresa){
            return(
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image
                            style={styles.logo}
                            source={require('../assets/modelo.png')}
                        />
                        {/* <Image
                            style={styles.logo}
                            source={{uri: api.getUri + empresa.photo}}
                        /> */}
                        <View style={styles.info}>
                            <Text style={[styles.headerText,{fontWeight: 'bold', fontSize: 20}]}>{empresa.enterprise_name}</Text>
                            <Text style={[styles.headerText]}>{empresa.city}, {empresa.country}</Text>
                            {empresa.enterprise_type ? (
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={[styles.headerText, {fontWeight: 'bold'}]}>Tipo: </Text>
                                    <Text style={[styles.headerText]}>{empresa.enterprise_type.enterprise_type_name}</Text>
                                </View>
                            ) : null}
                        </View>
                    </View>
                    <View style={styles.content}>
                        <Text style={{fontSize: 20, fontWeight:'bold', textAlign: 'center'}}>Sobre a empresa: </Text>
                        <View style={styles.descricao}>
                            <Text style={styles.descricaoText}>{empresa.description}</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                            <View style={styles.socialButton}>
                                <Button title="Facebook" color={Colors.facebook}/>    
                            </View>
                            <View style={styles.socialButton}>
                                <Button title="Linkedin" color={Colors.linkedin}/>    
                            </View>
                            <View style={styles.socialButton}>
                                <Button title="Twitter" color={Colors.twitter}/>    
                            </View>
                        </View>
                    </View>
                </View>
            );
        }
        else{
            return <ActivityIndicator />
        }
    }
}

function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(EmpresaInfoScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        margin: 10,
        backgroundColor: 'white'
    },
    header:{
        height: 100,
        flexDirection: 'row'
    },
    content:{
        flex: 1,
        flexDirection: 'column',
    },
    descricao: {
        flexDirection: 'column',
        borderColor: 'lightgray',
        borderWidth: 1,
        borderRadius: 5,
        margin: 5,
    },
    logo: {
        width: 100,
        height: 100,
    },
    info: {
        flexDirection: 'column',
        marginHorizontal: 10,
    },
    headerText:{
        fontSize: 16
    },
    descricaoText:{
        fontSize: 20,
        paddingHorizontal: 5,
        textAlign: 'justify'
    },
    socialButton: {
        margin: 5,
        padding: 5
    }
});