import React from 'react';
import {
    View,
    Text,
    Image,
    StyleSheet,
    ActivityIndicator
} from 'react-native';
import {connect} from 'react-redux';

import {default as Colors} from '../constants/Colors';

import api from '../services/api';
import AsyncStorage from '@react-native-community/async-storage';

class ContatoEmpresa extends React.Component{

    static navigationOptions = ({navigation}) => {
        return {
            headerStyle: {
                backgroundColor: Colors.primary,
            }
        };
    };

    constructor(props){
        super(props);
        this.state = {
            empresa: {}
        }
    }

    componentDidMount(){
        this.requestAsync();
    }

    requestAsync = async() => {
        const {access_token, client, uid} = this.props.auth;
        const id = this.props.navigation.getParam('id', 0);
        
        api.get('api/v1/enterprises/' + id,{
            headers: {
                'Content-Type': 'application/json',
                'access-token': access_token,
                'client': client,
                'uid': uid
            }
        })
        .then(res => {
            this.setState({
                empresa: res.data.enterprise
            })
        })
        .catch(error => {
            /**
             * Se o erro for 401 (Não autorizado), então as credencias não são mais
             * validas, logo o usuário deve realizar login novamente
             */
            if(error.response && error.response.status === 401){
                Alert.alert('Ops :(', 'Sua sessão expirou, faça login novamente !');
                this.props.navigation.navigate('Logout');
            }
        });
    }

    render(){
        const {empresa} = this.state;
        if(empresa){
            return(
                <View style={styles.container}>
                    <View style={styles.header}>
                        <Image
                            style={styles.logo}
                            source={require('../assets/modelo.png')}
                        />
                        {/* <Image
                            style={styles.logo}
                            source={{uri: api.getUri + empresa.photo}}
                        /> */}
                        <View style={styles.info}>
                            <Text style={[styles.headerText,{fontWeight: 'bold', fontSize: 20}]}>{empresa.enterprise_name}</Text>
                            <Text style={[styles.headerText]}>{empresa.city}, {empresa.country}</Text>
                            {empresa.enterprise_type ? (
                                <View style={{flexDirection: 'row'}}>
                                    <Text style={[styles.headerText, {fontWeight: 'bold'}]}>Tipo: </Text>
                                    <Text style={[styles.headerText]}>{empresa.enterprise_type.enterprise_type_name}</Text>
                                </View>
                            ) : null}
                        </View>
                    </View>
                    <View style={styles.content}>
                        <Text style={{fontSize: 20, fontWeight:'bold', textAlign: 'center'}}>Contatos: </Text>
                        {/* Fone */}
                        <View style={styles.linha}>
                            <View style={styles.label}>
                                <Text style={{fontWeight: 'bold', textAlign: 'right'}}>Telefone: </Text>
                            </View>
                            <View style={styles.contatoInfo}>
                                <Text>{empresa.phone}</Text>
                            </View>
                        </View>
                        {/* Email */}
                        <View style={styles.linha}>
                            <View style={styles.label}>
                                <Text style={{fontWeight: 'bold', textAlign: 'right'}}>E-mail: </Text>
                            </View>
                            <View style={styles.contatoInfo}>
                                <Text>{empresa.email}</Text>
                            </View>
                        </View>
                        {/* Facebook */}
                        <View style={styles.linha}>
                            <View style={styles.label}>
                                <Text style={{fontWeight: 'bold', textAlign: 'right'}}>Facebook: </Text>
                            </View>
                            <View style={styles.contatoInfo}>
                                <Text>{empresa.facebook}</Text>
                            </View>
                        </View>
                        {/* Twitter */}
                        <View style={styles.linha}>
                            <View style={styles.label}>
                                <Text style={{fontWeight: 'bold', textAlign: 'right'}}>Twitter: </Text>
                            </View>
                            <View style={styles.contatoInfo}>
                                <Text>{empresa.twitter}</Text>
                            </View>
                        </View>
                        {/* Linkdin */}
                        <View style={styles.linha}>
                            <View style={styles.label}>
                                <Text style={{fontWeight: 'bold', textAlign: 'right'}}>Linkedin: </Text>
                            </View>
                            <View style={styles.contatoInfo}>
                                <Text>{empresa.linkedin}</Text>
                            </View>
                        </View>
                    </View>
                </View>
            );
        }
        else{
            return <ActivityIndicator />
        }
    }
}

function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(ContatoEmpresa);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        margin: 10,
    },
    header:{
        height: 100,
        flexDirection: 'row'
    },
    content:{
        flex: 1,
        flexDirection: 'column',
    },
    descricao: {
        flexDirection: 'column',
        borderColor: 'lightgray',
        borderWidth: 1,
        borderRadius: 5,
        margin: 5,
    },
    logo: {
        width: 100,
        height: 100,
    },
    info: {
        flexDirection: 'column',
        marginHorizontal: 10,
    },
    headerText:{
        fontSize: 16
    },
    descricaoText:{
        fontSize: 20,
        paddingHorizontal: 5,
        textAlign: 'justify'
    },
    socialButton: {
        margin: 5,
        padding: 5
    },
    linha: {
        alignSelf: 'stretch', 
        flexDirection: 'row', 
        margin:5
    },
    label: {
        width: '20%',
        alignSelf: 'stretch'
    },
    contatoInfo:{
        width: '80%',
        alignSelf: 'stretch',
        borderColor: 'gray', 
        borderWidth: 1,
        borderRadius: 5
    }
});