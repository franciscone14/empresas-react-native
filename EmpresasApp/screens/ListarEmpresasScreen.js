import React from 'react';
import {
    View,
    FlatList,
    Button,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    Alert
} from 'react-native';
import {connect} from 'react-redux';

import {default as Colors} from '../constants/Colors';

import api from '../services/api';
import BasicInput from '../components/BasicInput';
import Ionicons from 'react-native-vector-icons/Ionicons';

class ListarEmpresasScreen extends React.Component{

    static navigationOptions = ({navigation}) => {
        return {
            title: 'Empresas',
            headerStyle: {
                backgroundColor: Colors.primary,
            },
            cardStyle: { backgroundColor: '#FFFFFF' },
            headerRight: () => (
                <TouchableOpacity style={{marginEnd: 10}} onPress={() => {
                    navigation.navigate('Logout');
                }}>
                    <Ionicons name="md-exit" size={20} color="black"/>
                </TouchableOpacity>
            ),
        };
    };

    constructor(props){
        super(props);
        this.state = {
            empresas: [],
            nome: '',
            tipo: ''
        }
    }

    componentDidMount(){
        this.requestAsync();
    }

    requestFilterAsync = async() => {
        const {access_token, client, uid} = this.props.auth;
        const {nome, tipo} = this.state;

        console.log(nome);

        if(nome === '' && tipo === ''){
            Alert.alert('Aviso !', 'Por favor, preencha pelo menos um dos campos de filtro !');
        }
        else{
            api.get('api/v1/enterprises?enterprise_types='+tipo+'&name=' + nome,{
                headers: {
                    'Content-Type': 'application/json',
                    'access-token': access_token,
                    'client': client,
                    'uid': uid
                }
            })
            .then(res => {
                this.setState({
                    empresas: res.data.enterprises,
                })
            })
            .catch(error => {
                /**
             * Se o erro for 401 (Não autorizado), então as credencias não são mais
             * validas, logo o usuário deve realizar login novamente
             */
            if(error.response && error.response.status === 401){
                Alert.alert('Ops :(', 'Sua sessão expirou, faça login novamente !');
                this.props.navigation.navigate('Logout');
            }
            });
        }
        
    }

    requestAsync = async() => {
        const {access_token, client, uid} = this.props.auth;

        api.get('api/v1/enterprises',{
            headers: {
                'Content-Type': 'application/json',
                'access-token': access_token,
                'client': client,
                'uid': uid
            }
        })
        .then(res => {
            this.setState({
                empresas: res.data.enterprises,
                tipo: '',
                nome: ''
            })
        })
        .catch(error => {
            /**
             * Se o erro for 401 (Não autorizado), então as credencias não são mais
             * validas, logo o usuário deve realizar login novamente
             */
            if(error.response && error.response.status === 401){
                Alert.alert('Ops :(', 'Sua sessão expirou, faça login novamente !');
                this.props.navigation.navigate('Logout');
            }
        });
    }

    onPressList = (id) => {
        this.props.navigation.navigate('EmpresaInfo', {id: id});
    }

    render(){
        const {empresas} = this.state;
        return(
            <View style={{flex:1, flexDirection: 'column'}}>
                <View style={{height: 100, flexDirection:'row', margin: 10}}>
                    <View style={{flex: 1}}>
                        <BasicInput
                            label="Nome: "
                            onChangeText={(nome) => this.setState({nome})}
                            value={this.state.nome}
                        />
                        <BasicInput
                            label="Tipo: "
                            onChangeText={(tipo) => this.setState({tipo})}
                            value={this.state.tipo}
                        />
                    </View>
                    <View style={{marginTop: 9}}>
                        <View style={{margin: 10}}>
                            <Button 
                                title="Search"
                                onPress={() => this.requestFilterAsync()}
                            />
                        </View>
                        <View style={{margin: 10}}>
                            <Button 
                                title="Limpar"
                                onPress={() => this.requestAsync()}
                                color="red"
                            />
                        </View>
                    </View>
                </View>
                <View style={{flex: 1}}>
                    <FlatList
                        data={empresas}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => this.onPressList(item.id)}>
                                <View style={styles.row}>
                                    <View style={styles.content}>
                                        {/* 
                                            Esse trecho deveria carregar a logo fornecidada pela url no json
                                            mas aparentemente nao esta funcionando
                                            <Image
                                                style={styles.logo}
                                                source={{uri: api.getUri + item.photo}}
                                            /> 
                                        */}
                                        <Image
                                            style={styles.logo}
                                            source={require('../assets/modelo.png')}
                                        />
                                        <View style={styles.info}>
                                            <Text style={styles.title}>{item.enterprise_name}</Text>
                                            <Text>{item.city}, {item.country}</Text>
                                        </View>
                                        <View style={{justifyContent: 'center', marginRight: 5}}>
                                            <Ionicons name="ios-arrow-forward" size={18} color="black"/>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )}
                        keyExtractor={item => item.id}
                    />
                </View>
            </View>
        );
    }
}

function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(ListarEmpresasScreen);

const styles = StyleSheet.create({
    row:{
        flex: 1,
        flexDirection: 'column',
        borderBottomColor: 'lightgray',
        borderBottomWidth: 1,
    },
    content:{
        flex: 1,
        flexDirection: 'row',
        margin: 5
    },
    info:{
        flex: 1,
        flexDirection: 'column'
    },
    logo: {
        width: 30, 
        height: 30,
        margin: 5
    },
    title:{
        fontWeight: 'bold',
        fontSize: 20
    }
});