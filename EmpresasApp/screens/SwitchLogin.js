import React from 'react';
import {
    ActivityIndicator
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';

class SwitchLogin extends React.Component{

    constructor(props){
        super(props);
        this.state = this.props.auth;
    }

    componentDidMount(){
        this.getStogareAsync();
    }

    /***
     * Busca os dados de autenticação no storage local, se ja existirem
     * o usuário pode usar o app, senão o usuario é redirecionado para o
     * login
     * 
     * O primeiro if verifica o estado do Redux, o segundo, caso a variavel exista
     * mas nao esta setado o estado no redux o set é feito e redicionado
     */
    getStogareAsync = async() => {
        const access_token = await AsyncStorage.getItem('@access_token');
        const client = await AsyncStorage.getItem('@client');
        const uid = await AsyncStorage.getItem('@uid');


        if(this.state.access_token !== ''){
            this.props.navigation.navigate('App');
        }
        else if(access_token !== '' && this.state.access_token === ''){
            this.props.dispatch({
                type: 'SET_STATE',
                access_token: access_token,
                client: client,
                uid: uid
            });
            this.props.navigation.navigate('App');
        }
        else{
            this.props.navigation.navigate('Login');
        }
    }

    // Indica que uma atividade está sendo computada
    render(){
        return(
            <ActivityIndicator />
        );
    }
}

function mapStateToProps(state){
    return {
        auth: state.auth
    }
}

export default connect(mapStateToProps)(SwitchLogin);