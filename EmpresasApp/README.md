# Desafio React Native - ioasys

Este documento `README.md` tem como objetivo fornecer as informações de como o desafio foi concluído

---

### Objetivo ###

* Desenvolver uma aplicação React Native que consuma a API `Empresas`, cujo Postman esta compartilhado neste repositório (collection).
* Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, **NÃO** é necessário criar um Pull Request para isso.
* Nós iremos realizar a avaliação e te retornar um email com o resultado.


### Dependências ###
* React Native - 0.61.5
* React Navigation - 4.0.10
    * Usado para contruir a interface de navegação do app
* Redux - 4.0.5
    * Usado para gerenciar o estado dos componentes e assim fornecer um maior controle de passagem de props e de atualização
    do estado
* React Redux - 7.1.3
    * Usado como middleware de conexão do React com o Redux
* React Native Community Async Storage - 1.7.1
    * Usado para armazenas os dados na memoria local do dispositivo (Tokens, Ids, etc)
* React Native Vector Icon - 6.6.0
    * Usado para carregar icones no app 
* Axios - 0.19.1
    * Usado para fazer as requisições na API
* Android Studio - Latest
    * Usado para instalar as dependencias de compilação do App
* Android Sdk 29 e ADB Tools
    * Usado para compilar e testar o App

* **OBS** As demais dependências no arquivo package.json são necessárias pelas bibliotecas anteriormente citadas.

### Como executar a aplicação ###
* No shell dentro da pasta do projeto:
```sh
    npm install
    npm start --reset-cache
```
* Em outro terminal também dentro da pasta do projeto:
```sh
    react-native run-android
```
* OBS: O uso desses comando parte do pressuposto que todas as dependências supracitadas estejam instaladas, bem como um dispositivo **ANDROID** esteja conectado a porta USB no modo depuração ou um emulador esteja instalado.

### Observações do candidato:
* Os links de midia retornados pela API não puderam ser carregados, pode ser devido a algum problema na API, ou por que
    como eu estou fora do Brasil a conexão está lenta com o servidor.

