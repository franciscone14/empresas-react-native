export default {
    primary: '#e51f6b',
    primaryLight: '#ff6099',
    primaryDark: '#ad0041',

    facebook: '#3b5998',
    linkedin: '#0077b5',
    twitter: '#1da1f2'
}