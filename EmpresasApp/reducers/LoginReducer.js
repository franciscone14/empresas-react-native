const initialState = {
    email: 'testeapple@ioasys.com.br',
    password: '12341234',
}

export default function loginReducer(state = initialState, action){
    switch(action.type){
        case 'SET_STATE_LOGIN': {
            return {
                ...state,
                email: action.email,
                password: action.password,
            }
        }
        default:
            return state;
    }
}