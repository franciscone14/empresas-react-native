const initialState = {
    access_token: '',
    client: '',
    uid: ''
}

export default function authReducer(state = initialState, action){
    switch(action.type){
        case 'SET_STATE': {
            return {
                ...state,
                access_token: action.access_token,
                client: action.client,
                uid: action.uid
            }
        }
        default:
            return state;
    }
}