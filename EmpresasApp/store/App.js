import { createStore, applyMiddleware, combineReducers } from "redux";

import thunk from "redux-thunk";
import loginReducer from "../reducers/LoginReducer";
import authReducer from "../reducers/AuthReducer";

const mainReducer = combineReducers({
    login: loginReducer,
    auth: authReducer
});

const store = createStore(mainReducer, applyMiddleware(thunk));

// const store = configureStore({ reducer: mainReducer })

export default store
