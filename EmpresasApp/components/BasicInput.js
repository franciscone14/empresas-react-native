import React from 'react';
import {
  View,
  Text,
  TextInput,
  StyleSheet
} from 'react-native';

import {default as Colors} from '../constants/Colors';
export default class BasicInput extends React.Component{
  constructor(props){
    super(props);
  }

  render(){
    return (
      <View style={styles.inputView}>
        <Text styles={styles.textInput}>{this.props.label}</Text>
        <TextInput
          style={styles.input}
          onChangeText={this.props.onChangeText}
          value={this.props.value}
          textContentType={this.props.textContentType}
          secureTextEntry={this.props.secureTextEntry}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({

  inputView:{
    height: 55,
    flexDirection: 'column'
  },
  input: {
    flex:1,
    borderColor: 'black',
    borderWidth: 1,
    // backgroundColor: Colors.primaryLight,
    color: 'black',
    fontSize:12
  },
  textInput:{

  }
});